package exercise;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Geometric {

	// 1. Given a choice of geometric figures, calculate the chosen figure�s
	// area and perimeter.
	// The required lengths will also be input by the user. The user can choose
	// between a circle,
	// a triangle or a rectangle. User input should be taken via console.

	void shape(double radius) {
		double perimeter = 2 * Math.PI * radius;
		System.out.printf("The perimeter is: %.2f", perimeter);
		System.out.println();
		double area = Math.PI * radius * radius;
		System.out.printf("The area is: %.2f", area);
	}

	void shape(double width, double height) {
		double perimeter = width + height;
		System.out.printf("The perimeter is: %.2f", perimeter);
		System.out.println();
		double area = width * height;
		System.out.printf("The area is: %.2f", area);

	}

	void shape(double sideOne, double sideTwo, double sideThree) {
		double perimeter = sideOne + sideTwo + sideThree;
		System.out.printf("The perimeter of the rectangle is : %.2f ", perimeter);
		System.out.println();
		double s = (sideOne + sideTwo + sideThree) / 2;
		double area = Math.sqrt(s * (s - sideOne) * (s - sideTwo) * (s - sideThree));
		System.out.printf("The area of the rectangle is : %.2f ", area);

	}

	void shapeOptions(String shapeType, Scanner scan) {
		switch (shapeType) {
		case "c":
			System.out.println("Type the radius of the circle: ");
			try {
				shape(scan.nextDouble());
			} catch (InputMismatchException e) {
				System.out.println("NaN or not enough values");
				System.out.println("Rerun the program to try again");
			}
			break;

		case "r":
			System.out.println("Type the width and height of the rectangle separated by a space");
			try {
				shape(scan.nextDouble(), scan.nextDouble());
			} catch (InputMismatchException e) {
				System.out.println("NaN or not enough values");
				System.out.println("Rerun the program to try again");
			}
			break;

		case "t":
			System.out.println("Type the all three sides of the triangle separated by space");

			try {
				shape(scan.nextDouble(), scan.nextDouble(), scan.nextDouble());
			} catch (InputMismatchException e) {
				System.out.println("NaN or not enough values");
				System.out.println("Rerun the program to try again");
			}

			break;
		default:
			System.out.println("Geometric form not supported yet. ");
			break;
		}
	}

	public static void main(String[] args) {

		System.out.println("Please type the first letter of the geometric"
				+ " form you want to calculate the perimeter and area: ");
		System.out.println("c: Circle");
		System.out.println("r: Rectangle");
		System.out.println("t: Triangle");
		Geometric geo = new Geometric();
		Scanner scan = new Scanner(System.in);
		scan.useDelimiter("\\s");
		String geometric = scan.nextLine().toLowerCase();
		geo.shapeOptions(geometric, scan);
		scan.close();
	}

}
